
# Phone Book Interface

* [Getting Started](#getting-started)

## Getting Started
1. Install [React](https://reactjs.org/docs/getting-started.html)

2. Install Node Modules
```
npm install
```
3. Run Application 
```
npm start
```
