import axios from 'axios';

export const GET_USERS = '[USERS APP] GET USERS';
export const SET_USERS_SEARCH_TEXT = '[USERS APP] SET USERS SEARCH TEXT';

export function getUsers()
{
    // TODO: change url
    const request = axios.get('http://www.mocky.io/v2/581335f71000004204abaf83');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_USERS,
                payload: response.data.contacts
            })
        );
}

export function setUsersSearchText(event)
{
    return {
        type      : SET_USERS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

