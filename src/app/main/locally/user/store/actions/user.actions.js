import axios from 'axios';
import { FuseUtils } from '@fuse';
import { showMessage } from 'app/store/actions/fuse';

export const GET_USER = '[USER APP] GET USER';
export const SAVE_USER = '[USER APP] SAVE USER';

export function getUser(params) {
	// TODO: Change url
	const request = axios.get('/api/e-commerce-app/product', { params });

	return (dispatch) =>
		request.then((response) =>
			dispatch({
				type: GET_USER,
				payload: response.data,
			}),
		);
}

export function saveUser(data) {
	// TODO: Change url
	const request = axios.post('/api/e-commerce-app/product/save', data);

	return (dispatch) =>
		request.then((response) => {
			dispatch(showMessage({ message: 'User Saved' }));

			return dispatch({
				type: SAVE_USER,
				payload: response.data,
			});
		});
}

export function newUser() {
    // TODO: Change data type
	const data = {
		id: FuseUtils.generateGUID(),
		name: '',
		handle: '',
		description: '',
		categories: [],
		tags: [],
		images: [],
		priceTaxExcl: 0,
		priceTaxIncl: 0,
		taxRate: 0,
		comparedPrice: 0,
		quantity: 0,
		sku: '',
		width: '',
		height: '',
		depth: '',
		weight: '',
		extraShippingFee: 0,
		active: true,
	};

	return {
		type: GET_USER,
		payload: data,
	};
}
