import React from 'react';
import { Redirect } from 'react-router-dom';
import { FuseUtils } from '@fuse/index';
import { locallyConfigs } from 'app/main/locally/locallyConfigs';

const routeConfigs = [...locallyConfigs];

const routes = [
	//if you want to make whole app auth protected by default change defaultAuth for example:
	// ...FuseUtils.generateRoutesFromConfigs(routeConfigs, ['admin','staff','user']),
	// The individual route configs which has auth option won't be overridden.
	...FuseUtils.generateRoutesFromConfigs(routeConfigs, null),
	{
		component: () => <Redirect to="/pages/errors/error-404" />,
	},
];

export default routes;
